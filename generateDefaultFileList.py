    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cPickle as pickle
from SIM.model import supportclasses
import os

thelist=[]


def relpathtoAVI(path):
	print path
	return os.path.relpath(path,"C:\Steam\skyrim")

relpaths=supportclasses.listAllFiles("C:\Steam\skyrim")

relpaths=map(relpathtoAVI,relpaths)
	
theList=[["Skyrim Game Files"]+relpaths]
print theList
afile=open("DefaultFileList.dat","wb")
pickle.dump(theList,afile)
afile.close()

print os.path.relpath("C:/Steam/skyrim/bom\TESV.exe","C:\Steam\skyrim")