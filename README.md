Skyrim Install Manager
======================

Introduction
------------
This program manages multiple Skyrim installations on one computer. It allows you to swap between these multiple installations easily, swapping the correct files in and out of the appropriate directories. Inactive installations are stored in a subdirectory of this application's working directory. A long term, primary goal is to make the application very safe to use; files from various installation must *never* be mixed up or deleted. It is intended to be easy to use, and .exe builds are released at Skyrim Nexus, at http://skyrim.nexusmods.com/mods/29307.

Dependencies
------------
The application is written in Python, and uses the wxPython library to draw the user interface. Besides wxPython, it uses only Python's standard libraries.

Usage/Building
------------
The application can be run as a Python script, under Python 2.7 with wxWidgets, or may be encapsulated into an executable using the build script in the *pyinstaller building* directory.

Licence
------------
This application is released under the GPL v3.0 licence.

![Screenshot of v0.5a](https://bitbucket.org/avihappy/skyrim-installation-manager/downloads/capture_0_5a.PNG "Screenshot of v0.5a")