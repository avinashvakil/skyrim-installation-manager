    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

import installation
import supportclasses
import installmanager

hotdir=supportclasses.HotDirectory({"APPDATA":"/Users/avinashvakil/Code/Skyrim Install Manager TestData/appdata 4 real"})
ignorefiles=supportclasses.DefaultFiles([["APPDATA","IMG_0163.jpg"]])
#skyriminstall=installation.SkyrimInstallation(hotdir,"/Users/avinashvakil/Code/Skyrim Install Manager TestData/store/install2",name="Unnamed")

installman=installmanager.InstallManager("/Users/avinashvakil/Code/Skyrim Install Manager TestData/managerinfo",ignorefiles,"/Users/avinashvakil/Code/Skyrim Install Manager TestData/store",hotdir)
#installman.createNewInstall("Term")
installman.activateInstallation("Term")

#print installman.installationObjectNames.keys()
#print installman.installationObjectNames.keys()

#print installman.installationObjectNames.keys()