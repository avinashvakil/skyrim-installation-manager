    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

import installation
import supportclasses
import os, shutil
import cPickle as pickle

class InstallManager(object):
	savedIvars=["activeInstall","defaultInstallStorage","listOfInstallsStorageDirs","firstRun"]
	"""
	This class represents a manager for the installations. It tracks them, 	activates or deactivates them, and feeds arguments they need to do their work. 	It also maintains the needed support classes. 
	"""
	def __init__(self,storageDir,unmovableFiles,defaultInstallStorage,defaultDirectories):
		"""
		storageDir is where settings are stored.
		
		defaultInstallStorage is a string with the location to store skyrim 		versions
		
		defaultDirectories is a HotDirectory object that stores the skyrim install 		directories
		
		unmovableFiles is an defaultFiles that stores the files to keep unmoved.
		"""
		self.storageDir=unicode(storageDir)
		self.unmovableFiles=unmovableFiles
		self.defaultDirectories=defaultDirectories
		self.installationObjects=[]
		self.installationObjectNames={}
		defaultInstallStorage=unicode(defaultInstallStorage)
		
		self.firstRun=True
		self.listOfInstallsStorageDirs=[os.path.join(defaultInstallStorage,"OriginalInstall")]
		self.activeInstall="OriginalInstall"
		self.defaultInstallStorage=defaultInstallStorage

		
		#Try to load the stored ivars
		for loadedVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,loadedVar+".SIMFILE"),"rb")
				self.__dict__[loadedVar]=pickle.load(afile)
				afile.close()
			except IOError:
				print loadedVar,"not loaded, will be ==", self.__dict__[loadedVar]
				
		self.defaultInstallStorage=unicode(self.defaultInstallStorage)
				
		#check to make sure each path is still valid, remove those that are not. Checks that each is unicode.
		for apath in self.listOfInstallsStorageDirs:
			if not isinstance(apath, unicode):
				self.listOfInstallsStorageDirs[self.listOfInstallsStorageDirs.index(apath)]=unicode(apath)

			if not os.path.exists(apath) and not self.firstRun:
				self.listOfInstallsStorageDirs.remove(apath)
				print "Could not find",apath
		
		#once we have correct and current values for the ivars, we can initialize the SkyrimInstallation objects.
		for apath in self.listOfInstallsStorageDirs:
			addedInstall=installation.SkyrimInstallation(self.defaultDirectories,apath,name="OriginalInstall") #we can assume that if we are making one here that doesn't exist already, it is the originalinstall. Time will tell if this is correct.
			self.installationObjects.append(addedInstall)
			self.installationObjectNames[addedInstall.name]=addedInstall

		if self.firstRun:
			self.installationObjectNames["OriginalInstall"].active=True
		
		self.wasFirstRun=False
		if self.firstRun:
			self.wasFirstRun=True
		self.firstRun=False
		self.storeState()
		
		print "initialized install MANAGER with data:"
		for variabl in self.__dict__.keys():
			print variabl,"==",self.__dict__[variabl]

		
	def createNewInstall(self,name):
		"""
		Allows a new install to be made in the default install storage directory, given a name.
		"""
		assert name not in self.installationObjectNames.keys(), "Name already used."
		apath=os.path.join(self.defaultInstallStorage,name)
		#the following code makes sure that the path is unique
		while os.path.exists(apath):
			apath=apath+'X'
		
		addedInstall=installation.SkyrimInstallation(self.defaultDirectories,apath,name=name)
		self.installationObjects.append(addedInstall)
		self.installationObjectNames[addedInstall.name]=addedInstall
		self.listOfInstallsStorageDirs.append(apath)
		self.storeState()
		
	def addMissingInstall(self,apath,name="Untitled"):
		if os.path.exists(apath):
			addedInstall=installation.SkyrimInstallation(self.defaultDirectories,apath,name=name) #we can assume that if we are making one here that doesn't exist already, it is the originalinstall. Time will tell if this is correct.
			assert addedInstall.name not in self.installationObjectNames.keys(), "Name already used."
			self.installationObjects.append(addedInstall)
			self.installationObjectNames[addedInstall.name]=addedInstall
			self.listOfInstallsStorageDirs.append(apath)
			self.storeState()
		else:
			print "Path does not exist"

	def duplicateInstallation(self,originalInstallName,newInstallName,progressCallback=None):
		assert newInstallName not in self.installationObjectNames.keys(), "Name already used."
		sourceItem=self.installationObjectNames[originalInstallName]

		#if there is a progress callback, we list and store the number of directories to operate on.
		if progressCallback!=None:
			listOfSubdirs=set(map(os.path.normpath,map(os.path.abspath,supportclasses.listAllDirs(sourceItem.storageDir)+[sourceItem.storageDir])))
			numOfSubdirs=len(listOfSubdirs)
		
		def ignoreFiles(directory,files):
			#if there is a function to send progress to, we do it here by removing the requested directory from the set and counting the length of the list of remaining directories.
			if progressCallback!=None:
				directoryAbsPath=os.path.normpath(os.path.abspath(directory))
				listOfSubdirs.remove(directoryAbsPath)
				progressCallback((1.0-float(len(listOfSubdirs))/numOfSubdirs)*100.0)
			return map(lambda direc:direc+u".SIMFILE",installation.SkyrimInstallation.savedIvars)
		
		newInstallPath=os.path.join(self.defaultInstallStorage,newInstallName)
		#the following code makes sure that the path is unique
		while os.path.exists(newInstallPath):
			newInstallPath=newInstallPath+'X'

		
		shutil.copytree(sourceItem.storageDir,newInstallPath,symlinks=False ,ignore=ignoreFiles)
		self.addMissingInstall(newInstallPath,name=newInstallName)
		
	def removeInstall(self,name,deleteFiles=False):
		assert name!=self.activeInstall, "Cannot delete active installation!"
		itemToRemove=self.installationObjectNames[name]
		itemToRemoveFiles=itemToRemove.storageDir
		del(self.installationObjectNames[name])
		self.listOfInstallsStorageDirs.remove(itemToRemove.storageDir)
		self.installationObjects.remove(itemToRemove)
		self.storeState()

		if deleteFiles:
			shutil.rmtree(itemToRemove.storageDir)

		
	def storeState(self):
		for saveVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,saveVar+".SIMFILE"),"wb")
				pickle.dump(self.__dict__[saveVar],afile)
				afile.close()
			except IOError:
				print saveVar,"failed to save as", self.__dict__[saveVar]
				
	def renameInstall(self,currentname,newname):
		"""
		Allows renaming of class. Assumes new name does not conflict so DO NOT CALL DIRECTLY. See installmanager class.
		"""
		assert currentname in self.installationObjectNames.keys(), "Cannot find installation."
		assert newname not in self.installationObjectNames.keys(), "Name already used." #Note, means you cannot rename to the same name.

		newInstallPath=os.path.join(self.defaultInstallStorage,newname)
		#the following code makes sure that the path is unique
		while os.path.exists(newInstallPath):
			newInstallPath=newInstallPath+'X'
		oldpath=self.installationObjectNames[currentname].storageDir
		self.installationObjectNames[currentname].rename(newname,newInstallPath)
		self.installationObjectNames[newname]=self.installationObjectNames[currentname]
		del(self.installationObjectNames[currentname])
		self.listOfInstallsStorageDirs.remove(oldpath)
		self.listOfInstallsStorageDirs.append(newInstallPath)
		self.storeState()
		
	def activateInstallation(self,name,callback=lambda x:None,errorResponder=lambda x:None):
		self.installationObjectNames[name]
		if self.activeInstall!='FAILSTATE':
			print "Attempting deactivation of", self.activeInstall
			deactivateReturnCode=self.installationObjectNames[self.activeInstall].deactivate(self.unmovableFiles,callback=callback)
			print "Deactivation",deactivateReturnCode
			if not deactivateReturnCode:
				errorResponder("deactivation")
			assert deactivateReturnCode, "Deactivation Failed. Make sure everything is closed properly!"
		print "Attempting activation of",name
		attemptedActivation=self.installationObjectNames[name].activate(self.unmovableFiles,callback=callback)
		print "Activation",attemptedActivation
		if attemptedActivation:
			self.activeInstall=name
		else:
			errorResponder("activation")
			self.activeInstall="FAILSTATE"
		self.storeState()
	
	def getActiveInstall(self):
		return self.activeInstall

