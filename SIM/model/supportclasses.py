    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

class HotDirectory(object):
	def __init__(self,directoryKeys):
		"""
		A HotDirectory object stores all important directories that are active, like user directory, appdata directory, and skyrim main data directories.
		
		It takes a a dict like so: {"APPDATA","/lol/appdata"}.
		"""
		assert isinstance(directoryKeys, dict)
		self.directories=directoryKeys
		
	def listHotDirTypes(self):
		return self.directories.keys()
		
	def getDirForType(self,dir):
		return self.directories[dir]
		
	def checkIfPathIsUnique(self,type,dir):
		import os.path
		pathChecked=os.path.abspath(dir)
		for typeKey in self.directories.keys():
			testPath=os.path.abspath(self.directories[typeKey])
			if type!=typeKey and (pathChecked.startswith(testPath) or testPath.startswith(pathChecked)):
				return False
		return True
		
	def overwriteDictionary(self,newdictionary):
		self.directories=newdictionary
		
class DefaultFiles(object):
	def __init__(self,defaultFiles):
		"""
		A HotDirectory object stores all vanilla files.
		
		It takes a dict like so: {"APPDATA",["/lol/appdata","123.txt"]}.
		"""
		self.files={}
		for alist in defaultFiles:
			self.files[alist[0]]=set(alist[1:]) #convert each item to a set for fast lookup.
		
	def isDefaultFile(self,filename,dirtype):
		return filename in self.files.get(dirtype,[])

import os
def listAllFiles(directoryToList):
	outputfiles=[]
	for root,dirs,files in os.walk(directoryToList):
		for file in files:
			outputfiles.append(os.path.join(root,file))
	return outputfiles
	
def listAllDirs(directoryToList):
	outputfiles=[]
	for root,dirs,files in os.walk(directoryToList):
		for file in dirs:
			outputfiles.append(os.path.join(root,file))
	return outputfiles

def getWindowsPathDefaults():
	output={}
	#get skyrim install path...
	try:
		import _winreg as registry
		localmachine=registry.ConnectRegistry(None,registry.HKEY_LOCAL_MACHINE)
		skyrimPathKey=registry.OpenKey(localmachine,r"SOFTWARE\Wow6432Node\Bethesda Softworks\Skyrim")
		
		skyrimPath=None
		index=0
		while skyrimPath==None:
			currentKey,currentValue,currentType=registry.EnumValue(skyrimPathKey,index)
			if currentKey=="Installed Path":
				skyrimPath=currentValue
			index+=1
		if os.path.exists(unicode(skyrimPath)):
			output["Skyrim Game Files"]=unicode(skyrimPath)
	except:
		print "Failed to autodetect Skyrim Game Files!"
		pass

	#get my documents path...
	try:
		from win32com.shell import shell, shellcon
		skyrimUserDataPath= os.path.join(shell.SHGetSpecialFolderPath(0, shellcon.CSIDL_PERSONAL),"My Games","Skyrim")
		if os.path.exists(unicode(skyrimUserDataPath)):
			output['Skyrim My Documents Folder']=unicode(skyrimUserDataPath)
	except:
		print "Failed to autodetect user files directory!"
	
	#get appdata path...
	try:
		from win32com.shell import shell, shellcon
		#get appdata folder...
		skyrimAppDataFolder= os.path.join(shell.SHGetSpecialFolderPath(0, shellcon.CSIDL_LOCAL_APPDATA),"Skyrim")
		if os.path.exists(unicode(skyrimAppDataFolder)):
			output['Skyrim AppData Files']=unicode(skyrimAppDataFolder)
	except:
		print "Failed to detect APPDATA directory."

	
	return output

def splitPath(aPath):
	output=[]
	remaining=aPath
	current="4"
	endSig=os.path.splitdrive(aPath)[0]
	while remaining!=endSig and current!="":
		remaining,current=os.path.split(remaining)
		output.append(current)
	return output
	
def pathOrder(aPath):
	return len(splitPath(aPath))
	
def deleteEmptySubdirs(topLevelDir):
	checkDirSortedByDepth=sorted(listAllDirs(topLevelDir),key=pathOrder,reverse=True)
	for aDir in checkDirSortedByDepth:
		if len(os.listdir(aDir))==0:
			try:
				os.rmdir(aDir)
			except:
				print "Failed to remove empty subdirectory", aDir
				pass
	return False