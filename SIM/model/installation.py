    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.


import cPickle as pickle
import random
import supportclasses
import os, shutil
class SkyrimInstallation(object):
	savedIvars=["uuid","name","active"]
	def __init__(self,HotDirectory,storageDir,name="Unnamed"):
		"""
		A SkyrimInstallation object represents a skyrim installation, with user directory, appdata directory, and skyrim main data directories.
		
		It takes a name, uuid, and a HotDirectory object.
		"""
		#Sets ivars as needed. Some of these will be overwritten when the stored ivars are loaded.
		self.name=name
		from datetime import datetime
		self.uuid=hash(str(datetime.now()))
		self.hotDirectory=HotDirectory
		self.storageDir=storageDir
		self.active=False
		
		#Try to make the storage directory if it does not exist
		if not os.path.exists(storageDir):
			os.mkdir(storageDir)
			print "Created missing primary installation storage directory",storageDir
		
		#Try to load the stored ivars
		for loadedVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,loadedVar+".SIMFILE"),"rb")
				self.__dict__[loadedVar]=pickle.load(afile)
				afile.close()
			except IOError:
				print loadedVar,"not loaded, will be ==", self.__dict__[loadedVar]

		
		#Will create required storage directoryies as defined by the HotDirectory object.
		for neededDir in self.hotDirectory.listHotDirTypes():
			if not os.path.exists(os.path.join(storageDir,neededDir)):
				os.mkdir(os.path.join(storageDir,neededDir))
				print "Created missing installation sub-directory for", neededDir, "at",storageDir
		
		#Finally, write the state.
		self.storeState()

		
		print "initialized install with data:"
		for variabl in self.__dict__.keys():
			print variabl,"==",self.__dict__[variabl]
		
	def activate(self,defaultFiles,callback=lambda x:None):
		#check if files lockable
		filesToMoveABSCHECK=[]
		for dirtype in self.hotDirectory.listHotDirTypes():
			filesToMoveABSCHECK+=supportclasses.listAllFiles(os.path.join(self.storageDir,dirtype))
		if not self.checkFileLocks(filesToMoveABSCHECK):
			return False
		
		print "Lock test passed!"
		
		numFiles=len(filesToMoveABSCHECK)
		movedSoFar=0
		
		#move if so
		failure=False
		for dirtype in self.hotDirectory.listHotDirTypes():
			filesToMoveABS=supportclasses.listAllFiles(os.path.join(self.storageDir,dirtype))
			for aFileToMove in filesToMoveABS:
				movedSoFar+=1
				progress=float(movedSoFar)/numFiles*100
				#print progress
				if movedSoFar%800==0:
					#print progress
					callback(progress)

				relfilename = os.path.relpath(aFileToMove,os.path.join(self.storageDir,dirtype))
				if not defaultFiles.isDefaultFile(relfilename,dirtype):
					destPath=os.path.join(self.hotDirectory.getDirForType(dirtype),relfilename)
					#print aFileToMove, "to", destPath
					if not os.path.exists(os.path.dirname(destPath)):
						os.makedirs(os.path.dirname(destPath))
					try:
						shutil.move(aFileToMove,destPath)
					except:
						print "=====> could not move", aFileToMove, "to",destPath
						failure=True
						
		if failure:
			self.deactivate(defaultFiles,callback=callback)
			return False
		self.active=True
		callback(100.0)
		return True
		#Move in my data and activate
		
	def deactivate(self,defaultFiles,callback=lambda x:None):
		#check if files lockable
		filesToMoveABSCHECK=[]
		for dirtype in self.hotDirectory.listHotDirTypes():
			filesToMoveABSCHECK+=supportclasses.listAllFiles(self.hotDirectory.getDirForType(dirtype))
		if not self.checkFileLocks(filesToMoveABSCHECK):
			return False

		print "Lock test passed!"
			
		numFiles=len(filesToMoveABSCHECK)
		movedSoFar=0

		failure=False
		#move files if so
		for dirtype in self.hotDirectory.listHotDirTypes():
			filesToMoveABS=supportclasses.listAllFiles(self.hotDirectory.getDirForType(dirtype))
			for aFileToMove in filesToMoveABS:
				progress=float(movedSoFar)/numFiles*100
				movedSoFar+=1
				if movedSoFar%800==0:
					#print progress
					callback(progress)

				relfilename = os.path.relpath(aFileToMove,self.hotDirectory.getDirForType(dirtype))
				if not defaultFiles.isDefaultFile(relfilename,dirtype):
					destPath=os.path.join(os.path.join(self.storageDir,dirtype),relfilename)
					#print aFileToMove, "to", destPath
					if not os.path.exists(os.path.dirname(destPath)):
						os.makedirs(os.path.dirname(destPath))
					try:
						shutil.move(aFileToMove,destPath)
					except:
						print "====> could not move", aFileToMove, "to",destPath
						failure=True
			#delete empty subdirectories...
			try:
				supportclasses.deleteEmptySubdirs(self.hotDirectory.getDirForType(dirtype))
			except:
				print "Failed to remove empty subdirectories for",self.hotDirectory.getDirForType(dirtype)
				pass
				
		if failure:
			self.activate(defaultFiles,callback=callback)
			return False
		self.active=False
		callback(100.0)
		return True

		
		#Move in my data and activate
	def checkFileLocks(self,fileList):
		import os
		import stat
		for afile in fileList:
			if not (os.stat(afile)[0] & stat.S_IWRITE):
				os.chmod(afile, stat.S_IWRITE)
				print afile,"was read only. Removed read only flag."
			try:
				attempted=os.open(afile,os.O_RDWR)
				os.close(attempted)
			except OSError:
				print "====> could not lock", afile
				return False
		return True
		#Move out my data back and deactivate. defaultFiles represents files to avoid copying back and forth.
		
	def storeState(self):
		for saveVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,saveVar+".SIMFILE"),"wb")
				pickle.dump(self.__dict__[saveVar],afile)
				afile.close()
			except IOError:
				print saveVar,"failed to save as", self.__dict__[saveVar]

	def rename(self,newname,newfolder):
		"""
		Allows renaming of class. Assumes new name does not conflict so DO NOT CALL DIRECTLY. See installmanager class.
		"""
		self.name=newname
		os.rename(self.storageDir,newfolder)
		self.storageDir=newfolder
		self.storeState()
		