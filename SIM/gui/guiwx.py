    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

import wx
from ..model import installation
from ..model import supportclasses
from ..model import installmanager

import os
import cPickle as pickle
class AppController(object):
	savedIvars=["skyrimFilesInitializer","storedFilesInitializer","managerStorageLocation"]
	def __init__(self):
		self.storageDir="saved configuration/appinfo"
		
		
		#self.defaultFilesInitializer=[["APPDATA","IMG_0163.jpg"]] #this will be dynamically loaded later...
		self.skyrimFilesInitializer={} #stores where different file locations are for skyrim.
		self.storedFilesInitializer="saved configuration/storage"
		self.managerStorageLocation="saved configuration/managerinfo"
				
		#Try to load the stored ivars
		for loadedVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,loadedVar+".SIMFILE"),"rb")
				self.__dict__[loadedVar]=pickle.load(afile)
				afile.close()
			except IOError:
				print loadedVar,"not loaded, will be ==", self.__dict__[loadedVar]

		self.storedFilesInitializer=unicode(self.storedFilesInitializer)
		self.managerStorageLocation=unicode(self.managerStorageLocation)
		#try to get the directories where skyrim is installed, if on windows and list is blank.
		if self.skyrimFilesInitializer=={} and os.name=='nt':
			self.skyrimFilesInitializer=supportclasses.getWindowsPathDefaults()

		for aLocation in self.skyrimFilesInitializer.keys():
			if not isinstance(self.skyrimFilesInitializer[aLocation],unicode):
				self.skyrimFilesInitializer[aLocation]=unicode(self.skyrimFilesInitializer[aLocation])

		defaultFilesList=open("DefaultFileList.dat","rb")
		self.defaultFilesInitializer=pickle.load(defaultFilesList)
		defaultFilesList.close()
		
		#Create paths if they are missing...
		if not os.path.exists(self.storageDir)	:
			os.makedirs(self.storageDir)
			
		if not os.path.exists(self.storedFilesInitializer)	:
			os.makedirs(self.storedFilesInitializer)
			
		if not os.path.exists(self.managerStorageLocation):
			os.makedirs(self.managerStorageLocation)


		self.directoryListObj=supportclasses.HotDirectory(self.skyrimFilesInitializer)
		self.defaultFilesObj=supportclasses.DefaultFiles(self.defaultFilesInitializer)
		
		#check if this program is in a bad location
		if not self.directoryListObj.checkIfPathIsUnique("",os.getcwd()):
			print "========================================================="
			print "DO NOT PLACE THIS PROGRAM INSIDE A GAME RELATED DIRECTORY"
			raw_input("PRESS ENTER TO QUIT")
			import sys
			sys.exit("PROGRAM INSIDE A WORKING DIRECTORY.")
		
		self.installman=installmanager.InstallManager(self.managerStorageLocation,self.defaultFilesObj,self.storedFilesInitializer,self.directoryListObj)
		self.storeState()
		
		
	def storeState(self):
		for saveVar in self.savedIvars:
			try:
				afile=open(os.path.join(self.storageDir,saveVar+".SIMFILE"),"wb")
				pickle.dump(self.__dict__[saveVar],afile)
				afile.close()
			except IOError:
				print saveVar,"failed to save as", self.__dict__[saveVar]
				
	def addToSkyrimFolders(self,label,path):
		assert self.directoryListObj.checkIfPathIsUnique(label,path), "Path is subdirectory of existing path!"
		self.skyrimFilesInitializer[label]=path
		#self.directoryListObj.overwriteDictionary(self.skyrimFilesInitializer)
		self.storeState()
		self.__init__()
		
	def removeFromSkyrimFolders(self,label):
		del(self.skyrimFilesInitializer[label])
		self.storeState()
		self.__init__()
		
	def createNewInstall(self,newName):
		self.installman.createNewInstall(newName)
		
	def deleteInstallation(self,newName,deleteFiles=False):
		self.installman.removeInstall(newName,deleteFiles=deleteFiles)

	def activateInstallation(self,installName,progressCallback=lambda x:None,errorResponder=lambda x:None):
		self.installman.activateInstallation(installName,callback=progressCallback,errorResponder=errorResponder)
		
	def getActiveInstallation(self):
		return self.installman.getActiveInstall()
		
	def duplicateInstallation(self,src,dst,progressCallback=None):
		self.installman.duplicateInstallation(src,dst,progressCallback=progressCallback)
		
	def renameInstallation(self,src,dst):
		self.installman.renameInstall(src,dst)
		
	def listDestDirectoryTypes(self):
		return self.directoryListObj.listHotDirTypes()
		
	def getDestDirForType(self,dirtype):
		return self.directoryListObj.getDirForType(dirtype)

		
		
		
class SIMMainFrame(wx.Frame):
	def __init__(self,parent,title,appcontroller):
		self.myparent=parent
		self.appcontroller=appcontroller
		wx.Frame.__init__(self, parent, title=title, size=(200,555))
		
		self.clearButton = wx.Button(self, label="Set As Active",pos=wx.Point(3, 400))
		self.Bind(wx.EVT_BUTTON, self.setActiveInstall, self.clearButton)
		
		self.newInstallButton = wx.Button(self, label="New Installation",pos=wx.Point(3, 430))
		self.Bind(wx.EVT_BUTTON, self.newInstall, self.newInstallButton)
		
		self.settingWindowButton = wx.Button(self, label="Settings",pos=wx.Point(3, 460))
		self.Bind(wx.EVT_BUTTON, self.showSettings, self.settingWindowButton)

		self.copyButton = wx.Button(self, label="Duplicate",pos=wx.Point(95, 400))
		self.Bind(wx.EVT_BUTTON, self.duplicateInstall, self.copyButton)
		
		self.copyButton = wx.Button(self, label="Rename",pos=wx.Point(95, 460))
		self.Bind(wx.EVT_BUTTON, self.renameInstall, self.copyButton)
		
		self.deleteButton = wx.Button(self, label="Delete",pos=wx.Point(95, 430))
		self.Bind(wx.EVT_BUTTON, self.deleteInstall, self.deleteButton)

		
		self.progBar=wx.Gauge(self, -1 ,100, pos=wx.Point(3, 490), size=(170, 25))
		
		self.optionsTable = wx.ListBox(self,size=wx.Size(299, 387),style=wx.LB_SINGLE)
		
		self.Show()
		self.updateUI()
		
		if self.appcontroller.installman.wasFirstRun:
			dlg = wx.MessageDialog(self, 'This is the first run of SIM. Please check that all your directories have been auto-detected correctly, as well as those of any utilities you use with the game. If you use Nexus Mod Manager, please add those directories as well!', 'Set your directories.', wx.OK|wx.ICON_INFORMATION)
			dlg.ShowModal()
			dlg.Destroy()
			self.showSettings(None)
		
	def showSettings(self,event):
		dialog= SIMSettingsFrame(self.myparent,"Settings",self.appcontroller)
		
	def setActiveInstall(self,event):
		index = self.optionsTable.GetSelection()
		if index>-1:
			selectedItem=self.currentlistofoptions[index]
			self.appcontroller.installman.activateInstallation(selectedItem,callback=self.setProgBar,errorResponder=self.printError)
			self.updateUI()
		dlg = wx.MessageDialog(self, selectedItem+' was activated successfully!', 'Activated', wx.OK|wx.ICON_INFORMATION)
		dlg.ShowModal()
		dlg.Destroy()

	def printError(self,error):
		if error=='deactivation':
			message="Could not deactivate the current profile. Switch operation aborted. Try closing any programs that may be using the game files."
		elif error=='activation':
			message="Could not activate the new profile. Currently, no profile is active."
		dlg = wx.MessageDialog(self, message, 'Failure', wx.OK|wx.ICON_ERROR)
		dlg.ShowModal()
		dlg.Destroy()

		
	def newInstall(self,event):
		dialog= wx.TextEntryDialog(self, 'Name your new installation.. Name it uniquely!','New Installation')
		if dialog.ShowModal() == wx.ID_OK:
			try:
				self.appcontroller.installman.createNewInstall(dialog.GetValue())
			except AssertionError:
				abox = wx.MessageDialog(self, 'This name is already used.', 'Invalid Name', wx.OK|wx.ICON_INFORMATION)
				abox.ShowModal()
				abox.Destroy()

			self.updateUI()
		dialog.Destroy()
		
	def renameInstall(self,event):
		index = self.optionsTable.GetSelection()
		if index>-1:
			selectedItem=self.currentlistofoptions[index]
			if self.appcontroller.installman.activeInstall==selectedItem:
				print "Cannot rename active installation!"
				abox = wx.MessageDialog(self, 'You cannot rename the active installation. Try activating another one first.', 'Invalid Choice', wx.OK|wx.ICON_INFORMATION)
				abox.ShowModal()
				abox.Destroy()

			else:
				dialog= wx.TextEntryDialog(self, 'Rename your installation.. Name it uniquely!','Rename Installation')
				if dialog.ShowModal() == wx.ID_OK:
					try:
						self.appcontroller.renameInstallation(selectedItem,dialog.GetValue())
					except AssertionError:
						abox = wx.MessageDialog(self, 'This name is already used.', 'Invalid Name', wx.OK|wx.ICON_INFORMATION)
						abox.ShowModal()
						abox.Destroy()
					self.updateUI()
				dialog.Destroy()

		
	def duplicateInstall(self,event):
		index = self.optionsTable.GetSelection()
		if index>-1:
			selectedItem=self.currentlistofoptions[index]
			if self.appcontroller.installman.activeInstall==selectedItem:
				print "Cannot duplicate active installation!"
				abox = wx.MessageDialog(self, 'You cannot duplicate the active installation. Try activating another one first.', 'Invalid Choice', wx.OK|wx.ICON_INFORMATION)
				abox.ShowModal()
				abox.Destroy()

			else:
				dialog= wx.TextEntryDialog(self, 'Name your duplicate installation.. Name it uniquely!','Duplicate Installation')
				if dialog.ShowModal() == wx.ID_OK:
					try:
						self.appcontroller.duplicateInstallation(selectedItem,dialog.GetValue(),self.setProgBar)
					except AssertionError:
						abox = wx.MessageDialog(self, 'This name is already used.', 'Invalid Name', wx.OK|wx.ICON_INFORMATION)
						abox.ShowModal()
						abox.Destroy()
					self.updateUI()
				dialog.Destroy()

	def deleteInstall(self,event):
		index = self.optionsTable.GetSelection()
		if index>-1:
			selectedItem=self.currentlistofoptions[index]
			if self.appcontroller.installman.activeInstall==selectedItem:
				print "Cannot delete active installation!"
				abox = wx.MessageDialog(self, 'You cannot delete the active installation. Try activating another one first.', 'Invalid Choice', wx.OK|wx.ICON_INFORMATION)
				abox.ShowModal()
				abox.Destroy()

			else:
				dialog= wx.TextEntryDialog(self, 'You wish to remove your install from the list. If you ALSO wish to physically delete the files, type DELETEMYFILES exactly. Otherwise, click OK.','Delete Installation')
				if dialog.ShowModal() == wx.ID_OK:
					try:
						deleteFiles=False
						if dialog.GetValue()=="DELETEMYFILES":
							deleteFiles=True
						self.appcontroller.deleteInstallation(selectedItem,deleteFiles)
					except AssertionError:
						pass
					self.updateUI()
				dialog.Destroy()

				
	def updateUI(self):
		self.optionsTable.Clear()
		self.currentlistofoptions= sorted(self.appcontroller.installman.installationObjectNames.keys())
		for anoption in self.currentlistofoptions:
			if anoption == self.appcontroller.installman.activeInstall:
				anoption+=" (ACTIVE)"
			self.optionsTable.Append(anoption)
		if self.appcontroller.installman.activeInstall!='FAILSTATE':
			self.optionsTable.SetSelection(self.currentlistofoptions.index(self.appcontroller.installman.activeInstall))
	
	def setProgBar(self,value):
		self.progBar.SetValue(value)
		wx.Yield()
	
class SIMSettingsFrame(wx.Frame):
	def __init__(self,parent,title,appcontroller):
		self.appcontroller=appcontroller
		wx.Frame.__init__(self, parent, title=title, size=wx.Size(500,400))
		
		self.clearButton = wx.Button(self, label="New Game Location",pos=wx.Point(3, 310))
		self.Bind(wx.EVT_BUTTON, self.newLocation, self.clearButton)
		
		self.newInstallButton = wx.Button(self, label="Remove Location",pos=wx.Point(3, 340))
		self.Bind(wx.EVT_BUTTON, self.deleteLocation, self.newInstallButton)
		
		self.pathView = wx.ListCtrl(self, -1, size=wx.Size(500,300),style=wx.LC_REPORT)
		#self.optionsTable = wx.ListBox(self,size=wx.Size(299, 387),style=wx.LB_SINGLE)
		self.pathView.InsertColumn(0, 'Type')
		self.pathView.InsertColumn(1, 'Location')
		self.pathView.SetColumnWidth(0, 140)
		self.pathView.SetColumnWidth(1, 360)		

		
		self.Show()
		self.updateUI()
				
	def newLocation(self,event):
		#dialog= wx.TextEntryDialog(self, 'Name your new installation.. Name it uniquely!','New Installation')
		#if dialog.ShowModal() == wx.ID_OK:
		#	nameItem=dialog.GetValue()
		#dialog.Destroy()
		
		#DEFINED NAME CHOICES.
		nameChoices = ['Skyrim AppData Files','Skyrim My Documents Folder','Skyrim Game Files','NXMM Files','NXMM Install Info','Wrye Bash Files','Extra 1','Extra 2','Extra 3']
		dialog=wx.SingleChoiceDialog(self, 'Choose a folder type to set:', 'Folder Type?', nameChoices, wx.CHOICEDLG_STYLE)
		if dialog.ShowModal() == wx.ID_OK:
			nameItem=dialog.GetStringSelection()
		dialog.Destroy()
		
		
		dialog2=wx.DirDialog(self, "Choose a folder:")
		if dialog2.ShowModal() == wx.ID_OK:
			pathItem=dialog2.GetPath()
		dialog2.Destroy()
		self.appcontroller.addToSkyrimFolders(nameItem,pathItem)
		self.updateUI()

	def deleteLocation(self,event):
		nameItem=self.pathView.GetNextSelected(-1)
		nameItem=self.listofdirtypes[nameItem]
		self.appcontroller.removeFromSkyrimFolders(nameItem)
		self.updateUI()

		
	def updateUI(self):
		self.listofdirtypes=self.appcontroller.directoryListObj.listHotDirTypes()
		self.pathView.DeleteAllItems()
		for dirtype in self.listofdirtypes:
			num_items=self.pathView.GetItemCount()
			self.pathView.InsertStringItem(num_items,dirtype)
			self.pathView.SetStringItem(num_items,1,self.appcontroller.directoryListObj.getDirForType(dirtype))

app = wx.App(False)
appController=AppController()
frame = SIMMainFrame(None, 'Skyrim Install Manager',appController)
app.MainLoop()