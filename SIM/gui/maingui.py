    # Skyrim Installation Manager - manage multiple skyrim installations
    # Copyright (C) 2013  Avinash Vakil

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

from Tkinter import *
from ..model import installation
from ..model import supportclasses
from ..model import installmanager


class SIMApp:
	def __init__(self, tkmaster):
			self.tkmaster=tkmaster
			frame = Frame(self.tkmaster)
			frame.pack()

			self.selectionbox=Listbox(frame,selectmode=SINGLE)
			self.selectionbox.pack(side=TOP)

			self.activeLabelTEXT = StringVar()
			self.activeLabel = Label(frame, textvariable=self.activeLabelTEXT)
			self.activeLabel.pack(side=TOP)

			self.configbutton = Button(frame, text="CONFIG", fg="red", command=frame.quit)
			self.configbutton.pack(side=LEFT)

			self.loadbutton = Button(frame, text="SELECT", command=self.activateSelectedItem)
			self.loadbutton.pack(side=LEFT)

			self.newbutton = Button(frame, text="NEW", command=self.createNewInstall)
			self.newbutton.pack(side=LEFT)

			self.importbutton = Button(frame, text="IMPORT", command=frame.quit)
			self.importbutton.pack(side=LEFT)
			
			
			self.createModel()
			self.updateUI()

	def createModel(self):
		self.directoryListObj=supportclasses.HotDirectory({"APPDATA":"/Users/avinashvakil/Code/Skyrim Install Manager TestData/appdata 4 real"})
		self.defaultFilesObj=supportclasses.DefaultFiles([["APPDATA","IMG_0163.jpg"]])
		self.installman=installmanager.InstallManager("/Users/avinashvakil/Code/Skyrim Install Manager TestData/managerinfo",self.defaultFilesObj,"/Users/avinashvakil/Code/Skyrim Install Manager TestData/store",self.directoryListObj)

	def updateUI(self):
		listSelections=self.installman.installationObjectNames.keys()
		self.selectionbox.delete(0,END)
		for item in listSelections:
			if item == self.installman.activeInstall:
				item+=" (ACTIVE)"
			self.selectionbox.insert(END, item)
		self.listOrder=listSelections
		self.activeLabelTEXT.set("Currently Active: "+self.installman.activeInstall)

	def activateSelectedItem(self):
		print self.selectionbox.curselection()[0]
		selectedName=self.listOrder[int(self.selectionbox.curselection()[0])]
		self.installman.activateInstallation(selectedName)
		self.updateUI()
		
	def createNewInstall(self):
		newDialog=CreateNewDialog(self.installman)
	
class CreateNewDialog:
	def __init__(self,installManager):
		top = Toplevel()
		
		self.inputBox = Text(top)
		self.inputBox.pack(side=TOP)

		
		#left off here

root = Tk()
app = SIMApp(root)
root.mainloop()
